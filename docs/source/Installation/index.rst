Installation Instructions
=========================

.. Attention:: At the moment **gym-DSSAT** only supports most common Linux distributions!

.. toctree::
   :maxdepth: 2

   packages
   docker
   spack
   source