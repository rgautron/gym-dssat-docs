.. _spack installation:

Spack installation
==================

`Spack <https://spack.io/>`_ is a package manager for easy sofwtare installation from Linux laptops to supercomputers.

Following the instructions below, installing **gym-DSSAT** is as easy as typing:

.. code-block:: shell

    spack install gym-dssat

Creating gym-dssat-pdi Spack environment
----------------------------------------

Step #1: Setup
^^^^^^^^^^^^^^

To use it, you should first setup spack:

.. code-block:: shell

    # 1. Get and enable Spack (version 0.19.0, last stable when writing this text)
    git clone --branch v0.19.0 -c feature.manyFiles=true https://github.com/spack/spack.git
    . spack/share/spack/setup-env.sh

    # 2. Get and enable PDI spack repo
    git clone https://github.com/pdidev/spack.git spack/var/spack/repos/pdi
    spack repo add spack/var/spack/repos/pdi

    # 3. Get and enable this spack repo
    git clone https://gitlab.inria.fr/rgautron/gym_dssat_pdi-spack.git spack/var/spack/repos/gym-dssat-pdi
    spack repo add spack/var/spack/repos/gym-dssat-pdi


Step #2: Installation
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: shell

    spack install gym-dssat

Step #3: Create gym-dssat-pdi Spack environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Spack environments make it easier to use the software installed via Spack. The recipe for the gym-dssat-pdi environment we have prepared is the file ``spack.yaml`` provided in ``spack/var/spack/repos/gym-dssat-pdi/env``. Once activated, this environment provides a specific view of the gym-dssat-pdi installation in ``spack/gymdssat_env``.

To create and activate the environment:

.. code-block:: shell

    # Create the Spack environment from the recipe
    spack env create gym-dssat-pdi spack/var/spack/repos/gym-dssat-pdi/env/spack.yaml

A view has been created for this environment in $HOME/gymdssat_spackenv

Using gym-dssat-pdi Spack environment
-------------------------------------

Step #1: Get easy access to Spack commands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this example, Spack has been installed in the user home: ``/home/emilioj/``.

.. code-block:: shell

    . /home/emilioj/spack/share/spack/setup-env.sh

Step #2A: Activate it
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: shell

    spack env activate gym-dssat-pdi

    You can check it is activated with
    spack env status

(Step #2B: Concretize the environment to get a unified/integrated 'view' of gym-dssat-pdi)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is only needed the first time or whenever we introduce changes in the environment!

.. code-block:: shell

    spack concretize

Step #3: Use gym-dssat-pdi Spack environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once we have an activated concretized environment we can use gym-dssat-pdi, in a similar way to using python venvs.

For example:

.. code-block:: shell

    $ which python
    /home/emilioj/spack/var/spack/environments/gym-dssat-pdi/.spack-env/view/bin/python

    $ python
    >>> import gym
    >>> env = gym.make('gym_dssat_pdi:GymDssatPdi-v0')

On the environment is activated, you can install additional Python packages you need with ``pip``.

We can also run the example bundled with gym-dssat:

.. code-block:: shell

    $ python gymdssat_spackenv/lib/python3.10/site-packages/gym_dssat_pdi_samples/run_env.py

Step #4: Dectivate it
^^^^^^^^^^^^^^^^^^^^^

To go back to your normal Python environment, type:

.. code-block:: shell

    spack env deactivate
