.. _preferred usage:

Preferred usage
===============

During its execution, **gym-DSSAT** writes files in a temporary folder and uses sockets through the TCP protocol. These elements require to be properly terminated.

To gracefully exit the environement, preferred usage is:

.. code-block:: python

    try:
        env = gym.make(...)
        ...
        env.step(action_dict=...)
        ...
    finally:
        env.close()


.. DANGER:: If your program crashes or if you skip the ``env.close`` step, the environment may not be properly terminated. As a consequence, you may experience zombie processes suceptible to interfer with new instances and cause bugs. If such case happens, you can kill the process by hand, for instance using ``pkill -9 dscsm048``.
