Rendering
=========

After a growing season is completed, **gym-DSSAT** provides a visualization interface both for raw state variables or rewards.

State
-----

To vizualise the time of state variables of a trajectory, you can call:

.. code-block:: python

    env.render(type='ts',  # time series mode
            feature_name_1='nstres',  # mandatory first raw state variable, here the nitrogen stress factor (unitless)
            feature_name_2='grnwt')  # optional second raw state variable, here the grain weight (kg/ha)

.. figure:: /_static/figures/nstresGrnwt.png
   :align: center

   Nitrogen stress (unitless) and grain yield (kg/ha) against Day Of Year (DOY)

.. Hint:: All variables in ``env._state`` are available for rendering. You can find the list and agronomical meaning in `gym environment's yaml configuration file <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/-/blob/stable/gym-dssat-pdi/gym_dssat_pdi/envs/configs/env_config.yml>`_ in the ``state`` key.

Reward
------

Raw reward
^^^^^^^^^^

For quick reward inspection, you can use:

.. code-block:: python

    env.render(type='reward')  # plot reward time series (DOY for Day Of Year)

.. figure:: /_static/figures/rewards.png
   :align: center

   Reward for a single trajectory against Day Of Year (DOY)

Cumulated reward
^^^^^^^^^^^^^^^^

Simply adding the ``cumsum=True`` argument, you can visualize the (undiscounted) cumulated reward.

.. code-block:: python

    env.render(type='reward', cumsum=True)  # if you want cumulated rewards

.. figure:: /_static/figures/cumsumRewards.png
   :align: center

   Cumulated reward for a single trajectory against Day Of Year (DOY)