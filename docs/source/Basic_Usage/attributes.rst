.. _attributes:

Handy attributes
================

**gym-dssat** provides observations to the agent as any RL environment ; but moreover the user can access additional hidden DSSAT's state features for the sake of the analysis.

``env._state`` and ``env.observation``
--------------------------------------

.. hint:: State and observation variables are defined as indicated in the :ref:`decision problems` section.

``env._state``
""""""""""""""

Contains all the raw state variables which is retrieved at every time step from DSSAT-PDI. For example, these variables can be used to inspect the simulations or plot dynamics.

``env.observation``
"""""""""""""""""""

Observations are subsets of ``env._state``. This corresponds to the agent perspective for the considered problem.

Histories
---------

Histories are dictionaries that are filled during each episode and which are emptied with ``env.reset()``. They are usefull for agent's performance analysis. Each key contains nested lists of the form:
``[[values_episode_1], ..., [values_episode_n]]``.

``env.history``
"""""""""""""""

.. code-block:: python

        self.history = {'observation': [...], 'action': [...], 'reward': [...]}

``env._history``
""""""""""""""""

.. code-block:: python

        self._history = {'state': [...], 'action': [...], 'reward': [...]}