Welcome to **gym-DSSAT** documentation!
#########################################

**gym-DSSAT** is a modification of the `Decision Support System for Agrotechnology Transfer <http://www.dssat.net/>`_ (DSSAT) Fortran software into an easy to manipulate Python `Open AI gym <https://gym.openai.com/>`_ environment for Reinforcement Learning (RL) researchers. **gym-DSSAT** allows daily based interactions during the growing season between an RL agent and the crop model with usual gym conventions. **gym-DSSAT** is proudly powered by the `PDI Data Interface (PDI) <https://gitlab.maisondelasimulation.fr/pdidev/pdi>`_ !

.. .. admonition:: New: Spack installation

..    With the :ref:`spack installation` **gym-DSSAT** is easily installed, no matters if it is on a Linux laptot or on a supercomputer!

.. Attention:: 
    At the moment **gym-DSSAT** only supports most common Linux distributions! If your distribution is not supported, you can still check our Docker images, see installation :ref:`docker images`.

.. toctree::
   :caption: User guide
   :titlesonly:
   :hidden:

   Installation/index
   Environment/index
   Basic_Usage/index
   Advanced_Usage/index
   Tutorials/index
   Baselines/index

Companion article
"""""""""""""""""

Please have a look to the `companion paper <https://hal.inria.fr/hal-03711132/>`_ which provides complementary information about gym-DSSAT, including use cases.

Citing gym-DSSAT
""""""""""""""""

.. code-block:: latex

    @phdthesis{gautron2022gym,
    title={gym-DSSAT: a crop model turned into a Reinforcement Learning environment},
    author={Gautron, Romain and Gonzalez, Emilio Jos{\'e} Padron and Preux, Philippe and Bigot, Julien and Maillard, Odalric-Ambrym and Emukpere, David},
    year={2022},
    school={Inria Lille}
    }

Help
""""

Any question/remark/request? The main support channel for **gym-DSSAT** is our `mailing list <https://sympa.inria.fr/sympa/info/gym-dssat>`_.

.. admonition:: Before asking a question

   Please check if you can find an answer in the `archives of our mail-list <https://sympa.inria.fr/sympa/arc/gym-dssat>`_.

Contributing
""""""""""""

Any `new issue <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/issues/new?title=Issue%20on%20page%20%2Findex.html&body=Your%20issue%20content%20here>`_ is welcomed! For the most motivated, we have a todo list!

Authors
"""""""

Romain Gautron and Emilio Padrón González

Acknowledgements
""""""""""""""""

We acknowledge the DSSAT team, especially Gerrit Hoogenboom and Cheryl Porter. Thanks to the PDI team, especially to Julien Bigot. We acknowledge Bruno Raffin, leader of Inria DataMove team, for his support. We acknowledge the Consultative Group for International Agricultural Research (CGIAR) Platform for Big Data in Agriculture, special thanks to Brian King. Thanks to the French Agricultural Research Centre for International Development (CIRAD) and the French Institute for Research in Computer Science and Automation (Inria), in particular the SCOOL team, for their support.