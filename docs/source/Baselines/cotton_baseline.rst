Cotton Baseline
===============

This section compares three different fertilization policies used in an experiment to improve crop yield.

   - The first policy is the **"null"** policy, which involves no fertilizer application, and is used as a reference point to measure the effect of nitrogen fertilizer on crop yield.
   - The second policy is the **"expert"** policy, which is the fertilizer application policy used in the original experiment. 
   - The third policy is a **"PPO"** policy, learned with default hyper-parameters.

.. note::
    The PPO policy was trained for :math:`10^6` episodes with stochastic weather generation, and the best model was saved based on performance in a validation environment.
    The performance of each policy was measured with :math:`10^3` episodes in a test environment; featuring different stochastic weather series from those used in training and evaluation environments of PPO, to avoid over-optimistic performance measures.

    Training and evaluation code can be found in the following `repository <https://gitlab.inria.fr/rgautron/gym_dssat_pdi_baselines/>`_
    
.. list-table:: 

    * - .. figure:: /_static/figures/CottonRewards.png

           Mean cumulated return of each of the 3 policies against the day of the simulation. Shaded area displays the [0.05, 0.95] quantile range for each policy.


      - .. figure:: /_static/figures/CottonApplications.png

           2D histogram of fertilizer applications (the darker the more frequent).



.. csv-table:: Grain Yield Experiment Results
   :header: "Policy", "Grain Yield (kg/ha)", "Percent nitrogen in leaf tissue (%)", "Total Fertilization (kg/ha)", "Application Number", "Nitrogen Use Efficiency (kg/kg)"
   :widths: 10, 20, 20, 20, 20, 20
   :class: transposed

   "Null", 365.1 (121.9),   1 (0.1),     **0 (0)**,     **0 (0)**,      "n.a."
   "Expert", **3199.4 (269.1)**,      **1.4 (0)**,       133 (0),         4 (0),        21.3 (1.3)
   "PPO", 2743.5 (341.9),       1.3 (0.1),      92.1 (16.4),       7.6 (2.1),    **26.1 (2.3)**