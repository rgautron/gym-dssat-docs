Baselines
=========

The current **gym-DSSAT** supports two cultivars : Maize and cotton.

.. note::
   As mentionned in the `companion paper <https://hal.inria.fr/hal-03711132/>`_, the goal was not to obtain the best performance, but to simply establish a baseline.

In these examples, we will adress the fertilization task. 
PPO was used with default hyper-parameters as set in ``stable-baselines3``.
Training and evaluation code can be found in the following `repository <https://gitlab.inria.fr/rgautron/gym_dssat_pdi_baselines/>`_.


.. toctree::
   :maxdepth: 2

   maize_baseline
   cotton_baseline

