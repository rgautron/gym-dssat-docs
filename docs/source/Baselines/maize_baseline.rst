Maize Baseline
==============

This section compares three different fertilization policies used in an experiment to improve crop yield.

   - The first policy is the **"null"** policy, which involves no fertilizer application, and is used as a reference point to measure the effect of nitrogen fertilizer on crop yield.
   - The second policy is the **"expert"** policy, which is the fertilizer application policy used in the original experiment. 
   - The third policy is a **"PPO"** policy, learned with default hyper-parameters.

.. note::
    The PPO policy was trained for :math:`10^6` episodes with stochastic weather generation, and the best model was saved based on performance in a validation environment.
    The performance of each policy was measured with :math:`10^3` episodes in a test environment; featuring different stochastic weather series from those used in training and evaluation environments of PPO, to avoid over-optimistic performance measures.

    Training and evaluation code can be found in the following `repository <https://gitlab.inria.fr/rgautron/gym_dssat_pdi_baselines/>`_.

.. Caution:: The results may be slightly different than the report due to stable-baselines3 different version and DSSAT engine update.

.. list-table:: 

    * - .. figure:: /_static/figures/MaizeRewards.png

           Mean cumulated return of each of the 3 policies against the day of the simulation. Shaded area displays the [0.05, 0.95] quantile range for each policy.


      - .. figure:: /_static/figures/MaizeApplications.png

           2D histogram of fertilizer applications (the darker the more frequent).



.. csv-table:: Grain Yield Experiment Results
   :header: "Policy", "Grain Yield (kg/ha)", "Massic nitrogen in grains (%)", "Total Fertilization (kg/ha)", "Application Number", "Nitrogen Use Efficiency (kg/kg)"
   :widths: 10, 20, 20, 20, 20, 20
   :class: transposed

   "Null", 1089.7 (369.8),   1.1 (0.1),     **0 (0)**,     **0 (0)**,      "n.a."
   "Expert", **3636.3 (1903)**,      **1.6 (0.3)**,       115.8 (5.2),         3 (0.1),        22 (14.3)
   "PPO", 3535.3 (1813.7),       1.5 (0.2),      98.7 (21),       3.5 (1.2),    **25.5 (16.7)**