Rewards
=======

| Reward functions are explicitely defined in a `separated Python file <https://gitlab.inria.fr/rgautron/gym_dssat_pdi/-/blob/stable/gym-dssat-pdi/gym_dssat_pdi/envs/configs/rewards.py>`_. For instance after package installation, you can simply edit: 
| ``${GYM_DSSAT_PDI_PATH}/envs/configs/rewards.py``

.. Hint:: Rewards are computed as functions of current and previous ``env._state``, see Section :ref:`attributes`.
